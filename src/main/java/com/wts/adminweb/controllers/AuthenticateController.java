package com.wts.adminweb.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wts.adminweb.dto.Response;
import com.wts.adminweb.dto.UserDTO;
import com.wts.adminweb.services.Utils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by nvtien on 10/4/18.
 */
@Controller
public class AuthenticateController {
    @Value(value = "${api-url}")
    private String apiURL;

    @GetMapping(value = "/login")
    public ModelAndView login(HttpSession session) {
        ModelAndView mav = new ModelAndView();
        if (session.getAttribute("user") != null) {
            mav.setViewName("redirect:/");
        } else {
            mav.addObject("user", new UserDTO());
            mav.setViewName("login");
        }

        return mav;
    }

    @PostMapping(value = "/loginProcess")
    public ModelAndView loginProcess(UserDTO userDTO, HttpSession session) throws IOException {
        ModelAndView mav = new ModelAndView();
        ObjectMapper objectMapper = new ObjectMapper();

        String loginResponse = Utils.postData(
                apiURL + "/login",
                objectMapper.writeValueAsString(userDTO)
        );

        Response<UserDTO> user = objectMapper.readValue(loginResponse, Response.class);
        if (user.isSuccess()) { // Login success
            session.setAttribute("user", userDTO);
            session.removeAttribute("error");
            mav.setViewName("redirect:/");
        } else { // Login failure
            session.setAttribute("error", user.getMessage());
            mav.setViewName("redirect:/login");
        }
        return mav;
    }
    @GetMapping(value = "/logout")
    public ModelAndView logout(HttpSession session) {
        ModelAndView mav = new ModelAndView();
        session.removeAttribute("user");
        mav.setViewName("redirect:/login");
        return mav;
    }
}
