package com.wts.adminweb.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wts.adminweb.dto.Response;
import com.wts.adminweb.dto.UserDTO;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by nvtien on 10/2/18.
 */
@Controller
public class ManageUserController {
    @Value(value = "${api-url}")
    private String apiURL;

    @GetMapping(value = "/")
    public ModelAndView listUser(HttpSession session) throws IOException {
        ModelAndView mav = new ModelAndView();
        if (session.getAttribute("user") == null) {
            mav.setViewName("redirect:/login");
            return mav;
        }
        String url = apiURL + "/users";

        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);

        BufferedReader br = new BufferedReader(
                new InputStreamReader((response.getEntity().getContent())));

        String output = br.readLine();

        ObjectMapper mapper = new ObjectMapper();
        Response<List<UserDTO>> users = mapper.readValue(output, Response.class);
        System.out.println(users);

        mav.addObject("users", users.getData());
        mav.setViewName("user");
        return mav;
    }
}
